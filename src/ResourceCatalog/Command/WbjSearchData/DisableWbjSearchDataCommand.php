<?php
namespace Sdk\ResourceCatalog\Command\WbjSearchData;

use Base\Sdk\ResourceCatalog\Command\WbjSearchData\DisableWbjSearchDataCommand as BaseDisableWbjSearchDataCommand;

class DisableWbjSearchDataCommand extends BaseDisableWbjSearchDataCommand
{
}
