<?php
namespace Sdk\ResourceCatalog\Command\GbSearchData;

use Base\Sdk\ResourceCatalog\Command\GbSearchData\DisableGbSearchDataCommand as BaseDisableGbSearchDataCommand;

class DisableGbSearchDataCommand extends BaseDisableGbSearchDataCommand
{
}
