<?php
namespace Sdk\ResourceCatalog\CommandHandler\WbjSearchData;

use Base\Sdk\ResourceCatalog\CommandHandler\WbjSearchData\WbjSearchDataCommandHandlerFactory
as BaseWbjSearchDataCommandHandlerFactory;

class WbjSearchDataCommandHandlerFactory extends BaseWbjSearchDataCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\ResourceCatalog\Command\WbjSearchData\DisableWbjSearchDataCommand'=>
            'Sdk\ResourceCatalog\CommandHandler\WbjSearchData\DisableWbjSearchDataCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
