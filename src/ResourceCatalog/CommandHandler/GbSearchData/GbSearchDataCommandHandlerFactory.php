<?php
namespace Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use Base\Sdk\ResourceCatalog\CommandHandler\GbSearchData\GbSearchDataCommandHandlerFactory
as BaseGbSearchDataCommandHandlerFactory;

class GbSearchDataCommandHandlerFactory extends BaseGbSearchDataCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\ResourceCatalog\Command\GbSearchData\DisableGbSearchDataCommand'=>
            'Sdk\ResourceCatalog\CommandHandler\GbSearchData\DisableGbSearchDataCommandHandler',
        'Sdk\ResourceCatalog\Command\GbSearchData\DeleteGbSearchDataCommand'=>
            'Sdk\ResourceCatalog\CommandHandler\GbSearchData\DeleteGbSearchDataCommandHandler',
        'Sdk\ResourceCatalog\Command\GbSearchData\ConfirmGbSearchDataCommand'=>
            'Sdk\ResourceCatalog\CommandHandler\GbSearchData\ConfirmGbSearchDataCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
