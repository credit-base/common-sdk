<?php
namespace Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use Base\Sdk\ResourceCatalog\CommandHandler\BjSearchData\BjSearchDataCommandHandlerFactory
as BaseBjSearchDataCommandHandlerFactory;

class BjSearchDataCommandHandlerFactory extends BaseBjSearchDataCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\ResourceCatalog\Command\BjSearchData\DisableBjSearchDataCommand'=>
            'Sdk\ResourceCatalog\CommandHandler\BjSearchData\DisableBjSearchDataCommandHandler',
        'Sdk\ResourceCatalog\Command\BjSearchData\DeleteBjSearchDataCommand'=>
            'Sdk\ResourceCatalog\CommandHandler\BjSearchData\DeleteBjSearchDataCommandHandler',
        'Sdk\ResourceCatalog\Command\BjSearchData\ConfirmBjSearchDataCommand'=>
            'Sdk\ResourceCatalog\CommandHandler\BjSearchData\ConfirmBjSearchDataCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
