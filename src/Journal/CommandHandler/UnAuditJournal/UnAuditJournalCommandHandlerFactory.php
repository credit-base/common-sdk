<?php
namespace Sdk\Journal\CommandHandler\UnAuditJournal;

use Base\Sdk\Journal\CommandHandler\UnAuditJournal\UnAuditJournalCommandHandlerFactory
as BaseUnAuditJournalCommandHandlerFactory;

class UnAuditJournalCommandHandlerFactory extends BaseUnAuditJournalCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Journal\Command\UnAuditJournal\EditUnAuditJournalCommand'=>
            'Sdk\Journal\CommandHandler\UnAuditJournal\EditUnAuditJournalCommandHandler',
        'Sdk\Journal\Command\UnAuditJournal\ApproveUnAuditJournalCommand'=>
            'Sdk\Journal\CommandHandler\UnAuditJournal\ApproveUnAuditJournalCommandHandler',
        'Sdk\Journal\Command\UnAuditJournal\RejectUnAuditJournalCommand'=>
            'Sdk\Journal\CommandHandler\UnAuditJournal\RejectUnAuditJournalCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
