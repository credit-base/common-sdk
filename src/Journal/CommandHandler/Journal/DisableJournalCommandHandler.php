<?php
namespace Sdk\Journal\CommandHandler\Journal;

use Base\Sdk\Journal\CommandHandler\Journal\DisableJournalCommandHandler as BaseDisableJournalCommandHandler;

class DisableJournalCommandHandler extends BaseDisableJournalCommandHandler
{
}
