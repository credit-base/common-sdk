<?php
namespace Sdk\Journal\CommandHandler\Journal;

use Base\Sdk\Journal\CommandHandler\Journal\JournalCommandHandlerFactory as BaseJournalCommandHandlerFactory;

class JournalCommandHandlerFactory extends BaseJournalCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Journal\Command\Journal\EditJournalCommand'=>
            'Sdk\Journal\CommandHandler\Journal\EditJournalCommandHandler',
        'Sdk\Journal\Command\Journal\AddJournalCommand'=>
            'Sdk\Journal\CommandHandler\Journal\AddJournalCommandHandler',
        'Sdk\Journal\Command\Journal\EnableJournalCommand'=>
            'Sdk\Journal\CommandHandler\Journal\EnableJournalCommandHandler',
        'Sdk\Journal\Command\Journal\DisableJournalCommand'=>
            'Sdk\Journal\CommandHandler\Journal\DisableJournalCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
