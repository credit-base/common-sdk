<?php
namespace Sdk\Rule\CommandHandler\Rule;

use Base\Sdk\Rule\CommandHandler\Rule\RuleCommandHandlerFactory as BaseRuleCommandHandlerFactory;

class RuleCommandHandlerFactory extends BaseRuleCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Rule\Command\Rule\EditRuleCommand'=>
            'Sdk\Rule\CommandHandler\Rule\EditRuleCommandHandler',
        'Sdk\Rule\Command\Rule\AddRuleCommand'=>
            'Sdk\Rule\CommandHandler\Rule\AddRuleCommandHandler',
        'Sdk\Rule\Command\Rule\DeleteRuleCommand'=>
            'Sdk\Rule\CommandHandler\Rule\DeleteRuleCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
