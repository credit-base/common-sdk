<?php
namespace Sdk\Rule\CommandHandler\UnAuditRule;

use Base\Sdk\Rule\CommandHandler\UnAuditRule\UnAuditRuleCommandHandlerFactory
as BaseUnAuditRuleCommandHandlerFactory;

class UnAuditRuleCommandHandlerFactory extends BaseUnAuditRuleCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Rule\Command\UnAuditRule\EditUnAuditRuleCommand'=>
            'Sdk\Rule\CommandHandler\UnAuditRule\EditUnAuditRuleCommandHandler',
        'Sdk\Rule\Command\UnAuditRule\ApproveUnAuditRuleCommand'=>
            'Sdk\Rule\CommandHandler\UnAuditRule\ApproveUnAuditRuleCommandHandler',
        'Sdk\Rule\Command\UnAuditRule\RejectUnAuditRuleCommand'=>
            'Sdk\Rule\CommandHandler\UnAuditRule\RejectUnAuditRuleCommandHandler',
        'Sdk\Rule\Command\UnAuditRule\RevokeUnAuditRuleCommand'=>
            'Sdk\Rule\CommandHandler\UnAuditRule\RevokeUnAuditRuleCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
