<?php
namespace Sdk\Interaction\CommandHandler\UnAuditAppeal;

use Base\Sdk\Interaction\CommandHandler\UnAuditAppeal\UnAuditAppealCommandHandlerFactory
as BaseUnAuditAppealCommandHandlerFactory;

class UnAuditAppealCommandHandlerFactory extends BaseUnAuditAppealCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Interaction\Command\UnAuditAppeal\ResubmitUnAuditAppealCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditAppeal\ResubmitUnAuditAppealCommandHandler',
        'Sdk\Interaction\Command\UnAuditAppeal\ApproveUnAuditAppealCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditAppeal\ApproveUnAuditAppealCommandHandler',
        'Sdk\Interaction\Command\UnAuditAppeal\RejectUnAuditAppealCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditAppeal\RejectUnAuditAppealCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
