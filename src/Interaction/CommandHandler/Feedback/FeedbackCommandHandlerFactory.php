<?php
namespace Sdk\Interaction\CommandHandler\Feedback;

use Base\Sdk\Interaction\CommandHandler\Feedback\FeedbackCommandHandlerFactory
as BaseFeedbackCommandHandlerFactory;

class FeedbackCommandHandlerFactory extends BaseFeedbackCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Interaction\Command\Feedback\AcceptFeedbackCommand'=>
            'Sdk\Interaction\CommandHandler\Feedback\AcceptFeedbackCommandHandler',
        'Sdk\Interaction\Command\Feedback\AddFeedbackCommand'=>
            'Sdk\Interaction\CommandHandler\Feedback\AddFeedbackCommandHandler',
        'Sdk\Interaction\Command\Feedback\PublishFeedbackCommand'=>
            'Sdk\Interaction\CommandHandler\Feedback\PublishFeedbackCommandHandler',
        'Sdk\Interaction\Command\Feedback\RevokeFeedbackCommand'=>
            'Sdk\Interaction\CommandHandler\Feedback\RevokeFeedbackCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
