<?php
namespace Sdk\Interaction\CommandHandler\UnAuditFeedback;

use Base\Sdk\Interaction\CommandHandler\UnAuditFeedback\UnAuditFeedbackCommandHandlerFactory
as BaseUnAuditFeedbackCommandHandlerFactory;

class UnAuditFeedbackCommandHandlerFactory extends BaseUnAuditFeedbackCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Interaction\Command\UnAuditFeedback\ResubmitUnAuditFeedbackCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditFeedback\ResubmitUnAuditFeedbackCommandHandler',
        'Sdk\Interaction\Command\UnAuditFeedback\ApproveUnAuditFeedbackCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditFeedback\ApproveUnAuditFeedbackCommandHandler',
        'Sdk\Interaction\Command\UnAuditFeedback\RejectUnAuditFeedbackCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditFeedback\RejectUnAuditFeedbackCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
