<?php
namespace Sdk\Interaction\CommandHandler\Complaint;

use Base\Sdk\Interaction\CommandHandler\Complaint\ComplaintCommandHandlerFactory
as BaseComplaintCommandHandlerFactory;

class ComplaintCommandHandlerFactory extends BaseComplaintCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Interaction\Command\Complaint\AcceptComplaintCommand'=>
            'Sdk\Interaction\CommandHandler\Complaint\AcceptComplaintCommandHandler',
        'Sdk\Interaction\Command\Complaint\AddComplaintCommand'=>
            'Sdk\Interaction\CommandHandler\Complaint\AddComplaintCommandHandler',
        'Sdk\Interaction\Command\Complaint\PublishComplaintCommand'=>
            'Sdk\Interaction\CommandHandler\Complaint\PublishComplaintCommandHandler',
        'Sdk\Interaction\Command\Complaint\RevokeComplaintCommand'=>
            'Sdk\Interaction\CommandHandler\Complaint\RevokeComplaintCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
