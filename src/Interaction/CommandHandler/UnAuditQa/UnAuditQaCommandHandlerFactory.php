<?php
namespace Sdk\Interaction\CommandHandler\UnAuditQa;

use Base\Sdk\Interaction\CommandHandler\UnAuditQa\UnAuditQaCommandHandlerFactory
as BaseUnAuditQaCommandHandlerFactory;

class UnAuditQaCommandHandlerFactory extends BaseUnAuditQaCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Interaction\Command\UnAuditQa\ResubmitUnAuditQaCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditQa\ResubmitUnAuditQaCommandHandler',
        'Sdk\Interaction\Command\UnAuditQa\ApproveUnAuditQaCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditQa\ApproveUnAuditQaCommandHandler',
        'Sdk\Interaction\Command\UnAuditQa\RejectUnAuditQaCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditQa\RejectUnAuditQaCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
