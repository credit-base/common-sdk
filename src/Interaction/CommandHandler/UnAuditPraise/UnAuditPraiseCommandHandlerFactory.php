<?php
namespace Sdk\Interaction\CommandHandler\UnAuditPraise;

use Base\Sdk\Interaction\CommandHandler\UnAuditPraise\UnAuditPraiseCommandHandlerFactory
as BaseUnAuditPraiseCommandHandlerFactory;

class UnAuditPraiseCommandHandlerFactory extends BaseUnAuditPraiseCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Interaction\Command\UnAuditPraise\ResubmitUnAuditPraiseCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditPraise\ResubmitUnAuditPraiseCommandHandler',
        'Sdk\Interaction\Command\UnAuditPraise\ApproveUnAuditPraiseCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditPraise\ApproveUnAuditPraiseCommandHandler',
        'Sdk\Interaction\Command\UnAuditPraise\RejectUnAuditPraiseCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditPraise\RejectUnAuditPraiseCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
