<?php
namespace Sdk\Interaction\CommandHandler\Qa;

use Base\Sdk\Interaction\CommandHandler\Qa\QaCommandHandlerFactory
as BaseQaCommandHandlerFactory;

class QaCommandHandlerFactory extends BaseQaCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Interaction\Command\Qa\AcceptQaCommand'=>
            'Sdk\Interaction\CommandHandler\Qa\AcceptQaCommandHandler',
        'Sdk\Interaction\Command\Qa\AddQaCommand'=>
            'Sdk\Interaction\CommandHandler\Qa\AddQaCommandHandler',
        'Sdk\Interaction\Command\Qa\PublishQaCommand'=>
            'Sdk\Interaction\CommandHandler\Qa\PublishQaCommandHandler',
        'Sdk\Interaction\Command\Qa\UnPublishQaCommand'=>
            'Sdk\Interaction\CommandHandler\Qa\UnPublishQaCommandHandler',
        'Sdk\Interaction\Command\Qa\RevokeQaCommand'=>
            'Sdk\Interaction\CommandHandler\Qa\RevokeQaCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
