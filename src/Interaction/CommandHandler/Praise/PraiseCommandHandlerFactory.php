<?php
namespace Sdk\Interaction\CommandHandler\Praise;

use Base\Sdk\Interaction\CommandHandler\Praise\PraiseCommandHandlerFactory
as BasePraiseCommandHandlerFactory;

class PraiseCommandHandlerFactory extends BasePraiseCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Interaction\Command\Praise\AcceptPraiseCommand'=>
            'Sdk\Interaction\CommandHandler\Praise\AcceptPraiseCommandHandler',
        'Sdk\Interaction\Command\Praise\AddPraiseCommand'=>
            'Sdk\Interaction\CommandHandler\Praise\AddPraiseCommandHandler',
        'Sdk\Interaction\Command\Praise\PublishPraiseCommand'=>
            'Sdk\Interaction\CommandHandler\Praise\PublishPraiseCommandHandler',
        'Sdk\Interaction\Command\Praise\UnPublishPraiseCommand'=>
            'Sdk\Interaction\CommandHandler\Praise\UnPublishPraiseCommandHandler',
        'Sdk\Interaction\Command\Praise\RevokePraiseCommand'=>
            'Sdk\Interaction\CommandHandler\Praise\RevokePraiseCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
