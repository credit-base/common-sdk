<?php
namespace Sdk\Interaction\CommandHandler\UnAuditComplaint;

use Base\Sdk\Interaction\CommandHandler\UnAuditComplaint\UnAuditComplaintCommandHandlerFactory
as BaseUnAuditComplaintCommandHandlerFactory;

class UnAuditComplaintCommandHandlerFactory extends BaseUnAuditComplaintCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Interaction\Command\UnAuditComplaint\ResubmitUnAuditComplaintCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditComplaint\ResubmitUnAuditComplaintCommandHandler',
        'Sdk\Interaction\Command\UnAuditComplaint\ApproveUnAuditComplaintCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditComplaint\ApproveUnAuditComplaintCommandHandler',
        'Sdk\Interaction\Command\UnAuditComplaint\RejectUnAuditComplaintCommand'=>
            'Sdk\Interaction\CommandHandler\UnAuditComplaint\RejectUnAuditComplaintCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
