<?php
namespace Sdk\Interaction\CommandHandler\Appeal;

use Base\Sdk\Interaction\CommandHandler\Appeal\AppealCommandHandlerFactory
as BaseAppealCommandHandlerFactory;

class AppealCommandHandlerFactory extends BaseAppealCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Interaction\Command\Appeal\AcceptAppealCommand'=>
            'Sdk\Interaction\CommandHandler\Appeal\AcceptAppealCommandHandler',
        'Sdk\Interaction\Command\Appeal\AddAppealCommand'=>
            'Sdk\Interaction\CommandHandler\Appeal\AddAppealCommandHandler',
        'Sdk\Interaction\Command\Appeal\PublishAppealCommand'=>
            'Sdk\Interaction\CommandHandler\Appeal\PublishAppealCommandHandler',
        'Sdk\Interaction\Command\Appeal\RevokeAppealCommand'=>
            'Sdk\Interaction\CommandHandler\Appeal\RevokeAppealCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
