<?php
namespace Sdk\Member\CommandHandler\Member;

use Base\Sdk\Member\CommandHandler\Member\MemberCommandHandlerFactory as BaseMemberCommandHandlerFactory;

class MemberCommandHandlerFactory extends BaseMemberCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Member\Command\Member\EnableMemberCommand'=>
            'Sdk\Member\CommandHandler\Member\EnableMemberCommandHandler',
        'Sdk\Member\Command\Member\DisableMemberCommand'=>
            'Sdk\Member\CommandHandler\Member\DisableMemberCommandHandler',
        'Sdk\Member\Command\Member\AddMemberCommand'=>
            'Sdk\Member\CommandHandler\Member\AddMemberCommandHandler',
        'Sdk\Member\Command\Member\EditMemberCommand'=>
            'Sdk\Member\CommandHandler\Member\EditMemberCommandHandler',
        'Sdk\Member\Command\Member\AuthMemberCommand'=>
            'Sdk\Member\CommandHandler\Member\AuthMemberCommandHandler',
        'Sdk\Member\Command\Member\ResetPasswordMemberCommand'=>
            'Sdk\Member\CommandHandler\Member\ResetPasswordMemberCommandHandler',
        'Sdk\Member\Command\Member\SignInMemberCommand'=>
            'Sdk\Member\CommandHandler\Member\SignInMemberCommandHandler',
        'Sdk\Member\Command\Member\SignOutMemberCommand'=>
            'Sdk\Member\CommandHandler\Member\SignOutMemberCommandHandler',
        'Sdk\Member\Command\Member\UpdatePasswordMemberCommand'=>
            'Sdk\Member\CommandHandler\Member\UpdatePasswordMemberCommandHandler',
        'Sdk\Member\Command\Member\ValidateSecurityMemberCommand'=>
            'Sdk\Member\CommandHandler\Member\ValidateSecurityMemberCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
