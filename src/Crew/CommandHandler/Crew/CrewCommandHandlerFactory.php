<?php
namespace Sdk\Crew\CommandHandler\Crew;

use Base\Sdk\Crew\CommandHandler\Crew\CrewCommandHandlerFactory as BaseCrewCommandHandlerFactory;

class CrewCommandHandlerFactory extends BaseCrewCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\Crew\Command\Crew\EditCrewCommand'=>
            'Sdk\Crew\CommandHandler\Crew\EditCrewCommandHandler',
        'Sdk\Crew\Command\Crew\SignInCrewCommand'=>
            'Sdk\Crew\CommandHandler\Crew\SignInCrewCommandHandler',
        'Sdk\Crew\Command\Crew\AddCrewCommand'=>
            'Sdk\Crew\CommandHandler\Crew\AddCrewCommandHandler',
        'Sdk\Crew\Command\Crew\SignOutCrewCommand'=>
            'Sdk\Crew\CommandHandler\Crew\SignOutCrewCommandHandler',
        'Sdk\Crew\Command\Crew\AuthCrewCommand'=>
            'Sdk\Crew\CommandHandler\Crew\AuthCrewCommandHandler',
        'Sdk\Crew\Command\Crew\EnableCrewCommand'=>
            'Sdk\Crew\CommandHandler\Crew\EnableCrewCommandHandler',
        'Sdk\Crew\Command\Crew\DisableCrewCommand'=>
            'Sdk\Crew\CommandHandler\Crew\DisableCrewCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
