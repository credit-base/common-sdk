<?php
namespace Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use Base\Sdk\WorkOrderTask\CommandHandler\WorkOrderTask\WorkOrderTaskCommandHandlerFactory as Base;

class WorkOrderTaskCommandHandlerFactory extends Base
{
    const MAPS = [
        'Sdk\WorkOrderTask\Command\WorkOrderTask\AssignWorkOrderTaskCommand'=>
            'Sdk\WorkOrderTask\CommandHandler\WorkOrderTask\AssignWorkOrderTaskCommandHandler',
        'Sdk\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand'=>
            'Sdk\WorkOrderTask\CommandHandler\WorkOrderTask\EndWorkOrderTaskCommandHandler',
        'Sdk\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand'=>
            'Sdk\WorkOrderTask\CommandHandler\WorkOrderTask\ConfirmWorkOrderTaskCommandHandler',
        'Sdk\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand'=>
            'Sdk\WorkOrderTask\CommandHandler\WorkOrderTask\FeedbackWorkOrderTaskCommandHandler',
        'Sdk\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand'=>
            'Sdk\WorkOrderTask\CommandHandler\WorkOrderTask\RevokeWorkOrderTaskCommandHandler',
    ];

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
