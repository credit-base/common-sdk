<?php
namespace Sdk\WorkOrderTask\CommandHandler\ParentTask;

use Base\Sdk\WorkOrderTask\CommandHandler\ParentTask\ParentTaskCommandHandlerFactory as Base;

class ParentTaskCommandHandlerFactory extends Base
{
    const MAPS = [
        'Sdk\WorkOrderTask\Command\ParentTask\AssignParentTaskCommand'=>
            'Sdk\WorkOrderTask\CommandHandler\ParentTask\AssignParentTaskCommandHandler',
        'Sdk\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand'=>
            'Sdk\WorkOrderTask\CommandHandler\ParentTask\RevokeParentTaskCommandHandler',
    ];

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
