<?php
namespace Sdk\News\CommandHandler\UnAuditNews;

use Base\Sdk\News\CommandHandler\UnAuditNews\UnAuditNewsCommandHandlerFactory as BaseUnAuditNewsCommandHandlerFactory;

class UnAuditNewsCommandHandlerFactory extends BaseUnAuditNewsCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\News\Command\UnAuditNews\EditUnAuditNewsCommand'=>
            'Sdk\News\CommandHandler\UnAuditNews\EditUnAuditNewsCommandHandler',
        'Sdk\News\Command\UnAuditNews\ApproveUnAuditNewsCommand'=>
            'Sdk\News\CommandHandler\UnAuditNews\ApproveUnAuditNewsCommandHandler',
        'Sdk\News\Command\UnAuditNews\RejectUnAuditNewsCommand'=>
            'Sdk\News\CommandHandler\UnAuditNews\RejectUnAuditNewsCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
