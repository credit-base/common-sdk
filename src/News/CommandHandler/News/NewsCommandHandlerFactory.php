<?php
namespace Sdk\News\CommandHandler\News;

use Base\Sdk\News\CommandHandler\News\NewsCommandHandlerFactory as BaseNewsCommandHandlerFactory;

class NewsCommandHandlerFactory extends BaseNewsCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\News\Command\News\EditNewsCommand'=>
            'Sdk\News\CommandHandler\News\EditNewsCommandHandler',
        'Sdk\News\Command\News\AddNewsCommand'=>
            'Sdk\News\CommandHandler\News\AddNewsCommandHandler',
        'Sdk\News\Command\News\EnableNewsCommand'=>
            'Sdk\News\CommandHandler\News\EnableNewsCommandHandler',
        'Sdk\News\Command\News\DisableNewsCommand'=>
            'Sdk\News\CommandHandler\News\DisableNewsCommandHandler',
        'Sdk\News\Command\News\TopNewsCommand'=>
            'Sdk\News\CommandHandler\News\TopNewsCommandHandler',
        'Sdk\News\Command\News\CancelTopNewsCommand'=>
            'Sdk\News\CommandHandler\News\CancelTopNewsCommandHandler',
        'Sdk\News\Command\News\MoveNewsCommand'=>
            'Sdk\News\CommandHandler\News\MoveNewsCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
