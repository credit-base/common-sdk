<?php
namespace Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Base\Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize\WebsiteCustomizeCommandHandlerFactory
as BaseWebsiteCustomizeCommandHandlerFactory;

class WebsiteCustomizeCommandHandlerFactory extends BaseWebsiteCustomizeCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand'=>
            'Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize\AddWebsiteCustomizeCommandHandler',
        'Sdk\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand'=>
            'Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize\PublishWebsiteCustomizeCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
