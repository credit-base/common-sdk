<?php
namespace Sdk\Template\CommandHandler\GbTemplate;

use Base\Sdk\Template\CommandHandler\GbTemplate\GbTemplateCommandHandlerFactory as BaseGbTemplateCommandHandlerFactory;

class GbTemplateCommandHandlerFactory extends BaseGbTemplateCommandHandlerFactory
{
    const MAPS = [
        'Sdk\Template\Command\GbTemplate\AddGbTemplateCommand'=>
            'Sdk\Template\CommandHandler\GbTemplate\AddGbTemplateCommandHandler',
        'Sdk\Template\Command\GbTemplate\EditGbTemplateCommand'=>
            'Sdk\Template\CommandHandler\GbTemplate\EditGbTemplateCommandHandler',
    ];

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
