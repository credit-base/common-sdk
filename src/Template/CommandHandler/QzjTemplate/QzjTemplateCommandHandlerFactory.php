<?php
namespace Sdk\Template\CommandHandler\QzjTemplate;

use Base\Sdk\Template\CommandHandler\QzjTemplate\QzjTemplateCommandHandlerFactory as BaseFactory;

class QzjTemplateCommandHandlerFactory extends BaseFactory
{
    const MAPS = [
        'Sdk\Template\Command\QzjTemplate\AddQzjTemplateCommand'=>
            'Sdk\Template\CommandHandler\QzjTemplate\AddQzjTemplateCommandHandler',
        'Sdk\Template\Command\QzjTemplate\EditQzjTemplateCommand'=>
            'Sdk\Template\CommandHandler\QzjTemplate\EditQzjTemplateCommandHandler',
    ];

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
