<?php
namespace Sdk\Template\CommandHandler\BaseTemplate;

use Base\Sdk\Template\CommandHandler\BaseTemplate\BaseTemplateCommandHandlerFactory
    as BaseBaseTemplateCommandHandlerFactory;

class BaseTemplateCommandHandlerFactory extends BaseBaseTemplateCommandHandlerFactory
{
    const MAPS = [
        'Sdk\Template\Command\BaseTemplate\EditBaseTemplateCommand'=>
            'Sdk\Template\CommandHandler\BaseTemplate\EditBaseTemplateCommandHandler',
    ];

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
