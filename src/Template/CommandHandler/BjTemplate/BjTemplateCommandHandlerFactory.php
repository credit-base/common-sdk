<?php
namespace Sdk\Template\CommandHandler\BjTemplate;

use Base\Sdk\Template\CommandHandler\BjTemplate\BjTemplateCommandHandlerFactory as BaseBjTemplateCommandHandlerFactory;

class BjTemplateCommandHandlerFactory extends BaseBjTemplateCommandHandlerFactory
{
    const MAPS = [
        'Sdk\Template\Command\BjTemplate\AddBjTemplateCommand'=>
            'Sdk\Template\CommandHandler\BjTemplate\AddBjTemplateCommandHandler',
        'Sdk\Template\Command\BjTemplate\EditBjTemplateCommand'=>
            'Sdk\Template\CommandHandler\BjTemplate\EditBjTemplateCommandHandler',
    ];

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
