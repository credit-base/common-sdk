<?php
namespace Sdk\Template\CommandHandler\WbjTemplate;

use Base\Sdk\Template\CommandHandler\WbjTemplate\WbjTemplateCommandHandlerFactory as BaseFactory;

class WbjTemplateCommandHandlerFactory extends BaseFactory
{
    const MAPS = [
        'Sdk\Template\Command\WbjTemplate\AddWbjTemplateCommand'=>
            'Sdk\Template\CommandHandler\WbjTemplate\AddWbjTemplateCommandHandler',
        'Sdk\Template\Command\WbjTemplate\EditWbjTemplateCommand'=>
            'Sdk\Template\CommandHandler\WbjTemplate\EditWbjTemplateCommandHandler',
    ];

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
