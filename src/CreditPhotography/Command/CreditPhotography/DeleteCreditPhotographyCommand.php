<?php
namespace Sdk\CreditPhotography\Command\CreditPhotography;

use Base\Sdk\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand
as BaseDeleteCreditPhotographyCommand;

class DeleteCreditPhotographyCommand extends BaseDeleteCreditPhotographyCommand
{
}
