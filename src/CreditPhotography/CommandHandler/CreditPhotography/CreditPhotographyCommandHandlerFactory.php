<?php
namespace Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use Base\Sdk\CreditPhotography\CommandHandler\CreditPhotography\CreditPhotographyCommandHandlerFactory
as BaseCreditPhotographyCommandHandlerFactory;

class CreditPhotographyCommandHandlerFactory extends BaseCreditPhotographyCommandHandlerFactory
{
    const MAPS = array(
        'Sdk\CreditPhotography\Command\CreditPhotography\ApproveCreditPhotographyCommand'=>
            'Sdk\CreditPhotography\CommandHandler\CreditPhotography\ApproveCreditPhotographyCommandHandler',
        'Sdk\CreditPhotography\Command\CreditPhotography\RejectCreditPhotographyCommand'=>
            'Sdk\CreditPhotography\CommandHandler\CreditPhotography\RejectCreditPhotographyCommandHandler',
        'Sdk\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand'=>
            'Sdk\CreditPhotography\CommandHandler\CreditPhotography\AddCreditPhotographyCommandHandler',
        'Sdk\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand'=>
            'Sdk\CreditPhotography\CommandHandler\CreditPhotography\DeleteCreditPhotographyCommandHandler',
    );

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
