<?php
namespace Sdk\UserGroup\CommandHandler;

use Base\Sdk\UserGroup\CommandHandler\DepartmentCommandHandlerFactory as BaseDepartmentCommandHandlerFactory;

class DepartmentCommandHandlerFactory extends BaseDepartmentCommandHandlerFactory
{
    const MAPS = [
        'Sdk\UserGroup\Command\AddDepartmentCommand'=>'Sdk\UserGroup\CommandHandler\AddDepartmentCommandHandler',
        'Sdk\UserGroup\Command\EditDepartmentCommand'=>'Sdk\UserGroup\CommandHandler\EditDepartmentCommandHandler',
    ];

    public function __construct()
    {
        parent::__construct(self::MAPS);
    }
}
