<?php
namespace Sdk\Journal\CommandHandler;

trait JournalCommandHandlerDataTrait
{
    public function getRequestCommonData() : array
    {
        $data = array(
            'title' => '多地出台评价标准',
            'source' => '北京发改委',
            "cover"=> array('name' => '封面名称', 'identify' => '封面地址.jpg'),
            "attachment"=> array('name' => '附件名称', 'identify' => '附件地址.pdf'),
            "authImages"=> array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg'),
                array('name' => 'name', 'identify' => 'identify.png')
            ),
            "description"=> "内容",
            "year"=> 2021,
            "status"=>0,
            "crew"=>1,
        );

        return $data;
    }
}
