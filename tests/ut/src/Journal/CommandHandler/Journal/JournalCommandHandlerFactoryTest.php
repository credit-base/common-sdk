<?php
namespace Sdk\Journal\CommandHandler\Journal;

use PHPUnit\Framework\TestCase;
use Sdk\Journal\Command\Journal\AddJournalCommand;
use Sdk\Journal\Command\Journal\EnableJournalCommand;
use Sdk\Journal\Command\Journal\DisableJournalCommand;

use Sdk\Journal\CommandHandler\JournalCommandHandlerDataTrait;

class JournalCommandHandlerFactoryTest extends TestCase
{
    use JournalCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new JournalCommandHandlerFactory();
    }

    public function testAddJournalCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new AddJournalCommand(
                $data['title'],
                $data['source'],
                $data['description'],
                $data['status'],
                $data['year'],
                $data['crew'],
                $data['cover'],
                $data['attachment'],
                $data['authImages']
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Journal\CommandHandler\Journal\AddJournalCommandHandler',
            $addCommandHandler
        );
    }

    public function testEnableJournalCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $commandHandler = $this->commandHandler->getHandler(
            new EnableJournalCommand(
                $data['crew'],
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Journal\CommandHandler\Journal\EnableJournalCommandHandler',
            $commandHandler
        );
    }

    public function testDisableJournalCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $commandHandler = $this->commandHandler->getHandler(
            new DisableJournalCommand(
                $data['crew'],
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Journal\CommandHandler\Journal\DisableJournalCommandHandler',
            $commandHandler
        );
    }
}
