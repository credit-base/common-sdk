<?php
namespace Sdk\Journal\CommandHandler\UnAuditJournal;

use PHPUnit\Framework\TestCase;
use Sdk\Journal\Command\UnAuditJournal\EditUnAuditJournalCommand;
use Sdk\Journal\Command\UnAuditJournal\ApproveUnAuditJournalCommand;
use Sdk\Journal\Command\UnAuditJournal\RejectUnAuditJournalCommand;

use Sdk\Journal\CommandHandler\JournalCommandHandlerDataTrait;

class UnAuditJournalCommandHandlerFactoryTest extends TestCase
{
    use JournalCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditJournalCommandHandlerFactory();
    }

    public function testEditUnAuditJournalCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new EditUnAuditJournalCommand(
                $data['title'],
                $data['source'],
                $data['description'],
                $data['status'],
                $data['year'],
                $data['crew'],
                $data['cover'],
                $data['attachment'],
                $data['authImages'],
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Journal\CommandHandler\UnAuditJournal\EditUnAuditJournalCommandHandler',
            $addCommandHandler
        );
    }

    public function testApproveUnAuditJournalCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveUnAuditJournalCommand(
                $data['crew'],
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Journal\CommandHandler\UnAuditJournal\ApproveUnAuditJournalCommandHandler',
            $commandHandler
        );
    }

    public function testRejectUnAuditJournalCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $commandHandler = $this->commandHandler->getHandler(
            new RejectUnAuditJournalCommand(
                '驳回原因',
                $data['crew'],
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Journal\CommandHandler\UnAuditJournal\RejectUnAuditJournalCommandHandler',
            $commandHandler
        );
    }
}
