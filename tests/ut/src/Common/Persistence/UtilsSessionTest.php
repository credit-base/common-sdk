<?php
namespace Sdk\Common\Persistence;

use PHPUnit\Framework\TestCase;

class UtilsSessionTest extends TestCase
{
    public function testCorrectExtendsSession()
    {
        $session = new UtilsSession();
        $this->assertInstanceof('Base\Sdk\Common\Persistence\UtilsSession', $session);
    }
}
