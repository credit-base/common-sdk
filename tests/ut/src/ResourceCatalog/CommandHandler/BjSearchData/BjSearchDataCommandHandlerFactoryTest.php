<?php
namespace Sdk\ResourceCatalog\CommandHandler\BjSearchData;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Command\BjSearchData\DisableBjSearchDataCommand;
use Sdk\ResourceCatalog\Command\BjSearchData\DeleteBjSearchDataCommand;
use Sdk\ResourceCatalog\Command\BjSearchData\ConfirmBjSearchDataCommand;

class BjSearchDataCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new BjSearchDataCommandHandlerFactory();
    }

    public function testBjSearchDataCommandHandlerFactory()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableBjSearchDataCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\CommandHandler\BjSearchData\DisableBjSearchDataCommandHandler',
            $commandHandler
        );
    }
}
