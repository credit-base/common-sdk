<?php
namespace Sdk\ResourceCatalog\CommandHandler\WbjSearchData;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Command\WbjSearchData\DisableWbjSearchDataCommand;

class WbjSearchDataCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new WbjSearchDataCommandHandlerFactory();
    }

    public function testWbjSearchDataCommandHandlerFactory()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableWbjSearchDataCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\CommandHandler\WbjSearchData\DisableWbjSearchDataCommandHandler',
            $commandHandler
        );
    }
}
