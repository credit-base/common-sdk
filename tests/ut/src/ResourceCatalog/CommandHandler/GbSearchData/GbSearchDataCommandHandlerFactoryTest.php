<?php
namespace Sdk\ResourceCatalog\CommandHandler\GbSearchData;

use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Command\GbSearchData\DisableGbSearchDataCommand;
use Sdk\ResourceCatalog\Command\GbSearchData\DeleteGbSearchDataCommand;
use Sdk\ResourceCatalog\Command\GbSearchData\ConfirmGbSearchDataCommand;

class GbSearchDataCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new GbSearchDataCommandHandlerFactory();
    }

    public function testGbSearchDataCommandHandlerFactory()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableGbSearchDataCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\CommandHandler\GbSearchData\DisableGbSearchDataCommandHandler',
            $commandHandler
        );
    }
}
