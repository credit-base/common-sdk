<?php
namespace Sdk\CreditPhotography\CommandHandler\CreditPhotography;

use PHPUnit\Framework\TestCase;
use Sdk\CreditPhotography\Command\CreditPhotography\ApproveCreditPhotographyCommand;
use Sdk\CreditPhotography\Command\CreditPhotography\RejectCreditPhotographyCommand;
use Sdk\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand;
use Sdk\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand;

class CreditPhotographyCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new CreditPhotographyCommandHandlerFactory();
    }

    public function testApproveCreditPhotographyCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveCreditPhotographyCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\CommandHandler\CreditPhotography\ApproveCreditPhotographyCommandHandler',
            $commandHandler
        );
    }

    public function testRejectCreditPhotographyCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RejectCreditPhotographyCommand(
                '驳回原因',
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\CommandHandler\CreditPhotography\RejectCreditPhotographyCommandHandler',
            $commandHandler
        );
    }

    public function testAddCreditPhotographyCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddCreditPhotographyCommand(
                $this->faker->word(),
                array(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\CommandHandler\CreditPhotography\AddCreditPhotographyCommandHandler',
            $commandHandler
        );
    }

    public function testDeleteCreditPhotographyCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DeleteCreditPhotographyCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\CommandHandler\CreditPhotography\DeleteCreditPhotographyCommandHandler',
            $commandHandler
        );
    }
}
