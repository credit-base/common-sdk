<?php
namespace Sdk\Interaction\CommandHandler\Praise;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Command\Praise\AcceptPraiseCommand;

use Sdk\Interaction\CommandHandler\InteractionCommandHandlerDataTrait;

class PraiseCommandHandlerFactoryTest extends TestCase
{
    use InteractionCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new PraiseCommandHandlerFactory();
    }

    public function testAcceptPraiseCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $acceptCommandHandler = $this->commandHandler->getHandler(
            new AcceptPraiseCommand(
                $data,
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $acceptCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Interaction\CommandHandler\Praise\AcceptPraiseCommandHandler',
            $acceptCommandHandler
        );
    }
}
