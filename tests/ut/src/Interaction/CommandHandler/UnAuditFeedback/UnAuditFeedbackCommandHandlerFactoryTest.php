<?php
namespace Sdk\Interaction\CommandHandler\UnAuditFeedback;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Command\UnAuditFeedback\ResubmitUnAuditFeedbackCommand;

use Sdk\Interaction\CommandHandler\InteractionCommandHandlerDataTrait;

class UnAuditFeedbackCommandHandlerFactoryTest extends TestCase
{
    use InteractionCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditFeedbackCommandHandlerFactory();
    }

    public function testResubmitUnAuditFeedbackCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new ResubmitUnAuditFeedbackCommand(
                $data,
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Interaction\CommandHandler\UnAuditFeedback\ResubmitUnAuditFeedbackCommandHandler',
            $addCommandHandler
        );
    }
}
