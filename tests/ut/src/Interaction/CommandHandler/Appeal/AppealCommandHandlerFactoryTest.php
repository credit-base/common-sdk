<?php
namespace Sdk\Interaction\CommandHandler\Appeal;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Command\Appeal\AcceptAppealCommand;

use Sdk\Interaction\CommandHandler\InteractionCommandHandlerDataTrait;

class AppealCommandHandlerFactoryTest extends TestCase
{
    use InteractionCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new AppealCommandHandlerFactory();
    }

    public function testAcceptAppealCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new AcceptAppealCommand(
                $data,
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Interaction\CommandHandler\Appeal\AcceptAppealCommandHandler',
            $addCommandHandler
        );
    }
}
