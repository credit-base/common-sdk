<?php
namespace Sdk\Interaction\CommandHandler\Qa;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Command\Qa\AcceptQaCommand;

use Sdk\Interaction\CommandHandler\InteractionCommandHandlerDataTrait;

class QaCommandHandlerFactoryTest extends TestCase
{
    use InteractionCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new QaCommandHandlerFactory();
    }

    public function testAcceptQaCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new AcceptQaCommand(
                $data,
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Interaction\CommandHandler\Qa\AcceptQaCommandHandler',
            $addCommandHandler
        );
    }
}
