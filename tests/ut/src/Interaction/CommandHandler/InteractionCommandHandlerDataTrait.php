<?php
namespace Sdk\Interaction\CommandHandler;

trait InteractionCommandHandlerDataTrait
{
    public function getRequestCommonData() : array
    {
        $data = array(
            'content' => '北京发改委',
            "images"=> array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg'),
                array('name' => 'name', 'identify' => 'identify.png')
            ),
            "admissibility"=> 1
        );

        return $data;
    }
}
