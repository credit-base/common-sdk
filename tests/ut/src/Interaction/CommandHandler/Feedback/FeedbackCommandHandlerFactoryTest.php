<?php
namespace Sdk\Interaction\CommandHandler\Feedback;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Command\Feedback\AcceptFeedbackCommand;

use Sdk\Interaction\CommandHandler\InteractionCommandHandlerDataTrait;

class FeedbackCommandHandlerFactoryTest extends TestCase
{
    use InteractionCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new FeedbackCommandHandlerFactory();
    }

    public function testAcceptFeedbackCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new AcceptFeedbackCommand(
                $data,
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Interaction\CommandHandler\Feedback\AcceptFeedbackCommandHandler',
            $addCommandHandler
        );
    }
}
