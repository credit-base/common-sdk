<?php
namespace Sdk\Interaction\CommandHandler\UnAuditPraise;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Command\UnAuditPraise\ResubmitUnAuditPraiseCommand;

use Sdk\Interaction\CommandHandler\InteractionCommandHandlerDataTrait;

class UnAuditPraiseCommandHandlerFactoryTest extends TestCase
{
    use InteractionCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditPraiseCommandHandlerFactory();
    }

    public function testResubmitUnAuditPraiseCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new ResubmitUnAuditPraiseCommand(
                $data,
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Interaction\CommandHandler\UnAuditPraise\ResubmitUnAuditPraiseCommandHandler',
            $addCommandHandler
        );
    }
}
