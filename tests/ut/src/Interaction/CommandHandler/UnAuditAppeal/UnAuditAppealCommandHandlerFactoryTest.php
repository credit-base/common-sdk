<?php
namespace Sdk\Interaction\CommandHandler\UnAuditAppeal;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Command\UnAuditAppeal\ResubmitUnAuditAppealCommand;

use Sdk\Interaction\CommandHandler\InteractionCommandHandlerDataTrait;

class UnAuditAppealCommandHandlerFactoryTest extends TestCase
{
    use InteractionCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditAppealCommandHandlerFactory();
    }

    public function testResubmitUnAuditAppealCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new ResubmitUnAuditAppealCommand(
                $data,
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Interaction\CommandHandler\UnAuditAppeal\ResubmitUnAuditAppealCommandHandler',
            $addCommandHandler
        );
    }
}
