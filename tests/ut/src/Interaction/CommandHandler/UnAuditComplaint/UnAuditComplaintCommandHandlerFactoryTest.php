<?php
namespace Sdk\Interaction\CommandHandler\UnAuditComplaint;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Command\UnAuditComplaint\ResubmitUnAuditComplaintCommand;

use Sdk\Interaction\CommandHandler\InteractionCommandHandlerDataTrait;

class UnAuditComplaintCommandHandlerFactoryTest extends TestCase
{
    use InteractionCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditComplaintCommandHandlerFactory();
    }

    public function testResubmitUnAuditComplaintCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new ResubmitUnAuditComplaintCommand(
                $data,
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Interaction\CommandHandler\UnAuditComplaint\ResubmitUnAuditComplaintCommandHandler',
            $addCommandHandler
        );
    }
}
