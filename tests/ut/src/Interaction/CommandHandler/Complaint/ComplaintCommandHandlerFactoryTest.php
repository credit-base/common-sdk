<?php
namespace Sdk\Interaction\CommandHandler\Complaint;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Command\Complaint\AcceptComplaintCommand;

use Sdk\Interaction\CommandHandler\InteractionCommandHandlerDataTrait;

class ComplaintCommandHandlerFactoryTest extends TestCase
{
    use InteractionCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new ComplaintCommandHandlerFactory();
    }

    public function testAcceptComplaintCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new AcceptComplaintCommand(
                $data,
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Interaction\CommandHandler\Complaint\AcceptComplaintCommandHandler',
            $addCommandHandler
        );
    }
}
