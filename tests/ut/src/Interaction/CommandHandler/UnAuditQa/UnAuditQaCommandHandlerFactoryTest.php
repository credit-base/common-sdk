<?php
namespace Sdk\Interaction\CommandHandler\UnAuditQa;

use PHPUnit\Framework\TestCase;
use Sdk\Interaction\Command\UnAuditQa\ResubmitUnAuditQaCommand;

use Sdk\Interaction\CommandHandler\InteractionCommandHandlerDataTrait;

class UnAuditQaCommandHandlerFactoryTest extends TestCase
{
    use InteractionCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditQaCommandHandlerFactory();
    }

    public function testResubmitUnAuditQaCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new ResubmitUnAuditQaCommand(
                $data,
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Interaction\CommandHandler\UnAuditQa\ResubmitUnAuditQaCommandHandler',
            $addCommandHandler
        );
    }
}
