<?php
namespace Sdk\Template\CommandHandler\BaseTemplate;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Command\BaseTemplate\EditBaseTemplateCommand;

class BaseTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new BaseTemplateCommandHandlerFactory();
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->commandHandler);
    }

    public function testBaseTemplateCommandHandlerFactory()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditBaseTemplateCommand(
                $this->faker->name,
                $this->faker->name,
                $this->faker->name,
                array(),
                array(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Template\CommandHandler\BaseTemplate\EditBaseTemplateCommandHandler',
            $commandHandler
        );
    }
}
