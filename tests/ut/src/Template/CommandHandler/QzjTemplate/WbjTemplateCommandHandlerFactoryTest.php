<?php
namespace Sdk\Template\CommandHandler\QzjTemplate;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Command\QzjTemplate\AddQzjTemplateCommand;

class QzjTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new QzjTemplateCommandHandlerFactory();
    }

    public function testQzjTemplateCommandHandlerFactory()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddQzjTemplateCommand(
                $this->faker->name,
                $this->faker->name,
                $this->faker->name,
                array(),
                array(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                21
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Template\CommandHandler\QzjTemplate\AddQzjTemplateCommandHandler',
            $commandHandler
        );
    }
}
