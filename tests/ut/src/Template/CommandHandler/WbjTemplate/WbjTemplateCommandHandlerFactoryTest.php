<?php
namespace Sdk\Template\CommandHandler\WbjTemplate;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Command\WbjTemplate\AddWbjTemplateCommand;

class WbjTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new WbjTemplateCommandHandlerFactory();
    }

    public function testWbjTemplateCommandHandlerFactory()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddWbjTemplateCommand(
                $this->faker->name,
                $this->faker->name,
                $this->faker->name,
                array(),
                array(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Template\CommandHandler\WbjTemplate\AddWbjTemplateCommandHandler',
            $commandHandler
        );
    }
}
