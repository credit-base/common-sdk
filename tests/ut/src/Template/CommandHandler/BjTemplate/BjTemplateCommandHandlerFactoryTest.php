<?php
namespace Sdk\Template\CommandHandler\BjTemplate;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Command\BjTemplate\AddBjTemplateCommand;

class BjTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new BjTemplateCommandHandlerFactory();
    }

    public function testBjTemplateCommandHandlerFactory()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddBjTemplateCommand(
                $this->faker->name,
                $this->faker->name,
                $this->faker->name,
                array(),
                array(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Template\CommandHandler\BjTemplate\AddBjTemplateCommandHandler',
            $commandHandler
        );
    }
}
