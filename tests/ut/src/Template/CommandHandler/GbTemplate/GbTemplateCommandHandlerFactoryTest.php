<?php
namespace Sdk\Template\CommandHandler\GbTemplate;

use PHPUnit\Framework\TestCase;
use Sdk\Template\Command\GbTemplate\AddGbTemplateCommand;

class GbTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new GbTemplateCommandHandlerFactory();
    }

    public function testGbTemplateCommandHandlerFactory()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddGbTemplateCommand(
                $this->faker->name,
                $this->faker->name,
                $this->faker->name,
                array(),
                array(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Template\CommandHandler\GbTemplate\AddGbTemplateCommandHandler',
            $commandHandler
        );
    }
}
