<?php
namespace Sdk\WorkOrderTask\CommandHandler\ParentTask;

use PHPUnit\Framework\TestCase;
use Sdk\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;

class ParentTaskCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new ParentTaskCommandHandlerFactory();
    }

    public function testRevokeParentTaskCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokeParentTaskCommand(
                $this->faker->randomNumber(),
                $this->faker->title
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\WorkOrderTask\CommandHandler\ParentTask\RevokeParentTaskCommandHandler',
            $commandHandler
        );
    }
}
