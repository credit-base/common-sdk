<?php
namespace Sdk\WorkOrderTask\CommandHandler\WorkOrderTask;

use PHPUnit\Framework\TestCase;
use Sdk\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand;

class WorkOrderTaskCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new WorkOrderTaskCommandHandlerFactory();
    }

    public function testConfirmWorkOrderTaskCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ConfirmWorkOrderTaskCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\WorkOrderTask\CommandHandler\WorkOrderTask\ConfirmWorkOrderTaskCommandHandler',
            $commandHandler
        );
    }
}
