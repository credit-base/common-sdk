<?php
namespace Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Sdk\WebsiteCustomize\Command\WebsiteCustomize\AddWebsiteCustomizeCommand;
use Sdk\WebsiteCustomize\Command\WebsiteCustomize\PublishWebsiteCustomizeCommand;

use Base\Sdk\Crew\Model\Crew;

class WebsiteCustomizeCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        Core::$container->set('crew', new Crew(1));
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new WebsiteCustomizeCommandHandlerFactory();
    }

    public function testPublishWebsiteCustomizeCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new PublishWebsiteCustomizeCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize\PublishWebsiteCustomizeCommandHandler',
            $commandHandler
        );
    }

    public function testAddWebsiteCustomizeCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddWebsiteCustomizeCommand(
                1,
                2,
                array(1,2)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\WebsiteCustomize\CommandHandler\WebsiteCustomize\AddWebsiteCustomizeCommandHandler',
            $commandHandler
        );
    }
}
