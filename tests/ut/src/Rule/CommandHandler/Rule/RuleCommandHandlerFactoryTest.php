<?php
namespace Sdk\Rule\CommandHandler\Rule;

use PHPUnit\Framework\TestCase;
use Sdk\Rule\Command\Rule\AddRuleCommand;
use Sdk\Rule\Command\Rule\EditRuleCommand;
use Sdk\Rule\Command\Rule\DeleteRuleCommand;

use Sdk\Rule\CommandHandler\RuleCommandHandlerDataTrait;

class RuleCommandHandlerFactoryTest extends TestCase
{
    use RuleCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new RuleCommandHandlerFactory();
    }

    public function testAddRuleCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new AddRuleCommand(
                $data['rules'],
                $data['transformationTemplate'],
                $data['sourceTemplate'],
                1,
                2
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Rule\CommandHandler\Rule\AddRuleCommandHandler',
            $addCommandHandler
        );
    }

    public function testEditRuleCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new EditRuleCommand(
                $data['rules'],
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Rule\CommandHandler\Rule\EditRuleCommandHandler',
            $addCommandHandler
        );
    }

    public function testDeleteRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DeleteRuleCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Rule\CommandHandler\Rule\DeleteRuleCommandHandler',
            $commandHandler
        );
    }
}
