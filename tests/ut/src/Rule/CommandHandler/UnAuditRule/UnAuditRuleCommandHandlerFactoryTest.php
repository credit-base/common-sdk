<?php
namespace Sdk\Rule\CommandHandler\UnAuditRule;

use PHPUnit\Framework\TestCase;
use Sdk\Rule\Command\UnAuditRule\EditUnAuditRuleCommand;
use Sdk\Rule\Command\UnAuditRule\ApproveUnAuditRuleCommand;
use Sdk\Rule\Command\UnAuditRule\RejectUnAuditRuleCommand;
use Sdk\Rule\Command\UnAuditRule\RevokeUnAuditRuleCommand;

use Sdk\Rule\CommandHandler\RuleCommandHandlerDataTrait;

class UnAuditRuleCommandHandlerFactoryTest extends TestCase
{
    use RuleCommandHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditRuleCommandHandlerFactory();
    }

    public function testEditUnAuditRuleCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new EditUnAuditRuleCommand(
                $data['rules'],
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\Rule\CommandHandler\UnAuditRule\EditUnAuditRuleCommandHandler',
            $addCommandHandler
        );
    }

    public function testApproveUnAuditRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveUnAuditRuleCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Rule\CommandHandler\UnAuditRule\ApproveUnAuditRuleCommandHandler',
            $commandHandler
        );
    }

    public function testRejectUnAuditRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RejectUnAuditRuleCommand(
                '驳回原因',
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Rule\CommandHandler\UnAuditRule\RejectUnAuditRuleCommandHandler',
            $commandHandler
        );
    }

    public function testRevokeUnAuditRuleCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokeUnAuditRuleCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Rule\CommandHandler\UnAuditRule\RevokeUnAuditRuleCommandHandler',
            $commandHandler
        );
    }
}
