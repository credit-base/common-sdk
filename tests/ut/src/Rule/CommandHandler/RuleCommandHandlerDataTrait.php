<?php
namespace Sdk\Rule\CommandHandler;

trait RuleCommandHandlerDataTrait
{
    public function getRequestCommonData() : array
    {
        $data = array(
            'rules' => array(1,2),
            'type' => 1,
            "sourceTemplate"=> 1,
            "transformationTemplate"=>1,
            "crew"=>1,
        );

        return $data;
    }
}
