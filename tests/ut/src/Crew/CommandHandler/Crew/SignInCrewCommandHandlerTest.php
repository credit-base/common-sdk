<?php
namespace Sdk\Crew\CommandHandler\Crew;

use PHPUnit\Framework\TestCase;

class SignInCrewCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new SignInCrewCommandHandler();
        $this->assertInstanceof('Base\Sdk\Crew\CommandHandler\Crew\SignInCrewCommandHandler', $commandHandler);
    }
}
