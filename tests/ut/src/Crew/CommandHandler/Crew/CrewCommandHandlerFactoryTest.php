<?php
namespace Sdk\Crew\CommandHandler\Crew;

use PHPUnit\Framework\TestCase;
use Sdk\Crew\Command\Crew\SignInCrewCommand;

class CrewCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new CrewCommandHandlerFactory();
    }

    public function testSignInCrewCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new SignInCrewCommand(
                $this->faker->name(),
                $this->faker->name()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Crew\CommandHandler\Crew\SignInCrewCommandHandler',
            $commandHandler
        );
    }
}
