<?php
namespace Sdk\Crew\CommandHandler\Crew;

use PHPUnit\Framework\TestCase;

class EditCrewCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new EditCrewCommandHandler();
        $this->assertInstanceof('Base\Sdk\Crew\CommandHandler\Crew\EditCrewCommandHandler', $commandHandler);
    }
}
