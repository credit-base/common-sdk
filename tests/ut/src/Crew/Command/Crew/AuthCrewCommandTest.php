<?php
namespace Sdk\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class AuthCrewCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $id = 1;
        $identify = 'identify';

        $command = new AuthCrewCommand($id, $identify);
        $this->assertInstanceof('Base\Sdk\Crew\Command\Crew\AuthCrewCommand', $command);
    }
}
