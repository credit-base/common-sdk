<?php
namespace Sdk\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class SignOutCrewCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $command = new SignOutCrewCommand();
        $this->assertInstanceof('Base\Sdk\Crew\Command\Crew\SignOutCrewCommand', $command);
    }
}
