<?php
namespace Sdk\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class EditCrewCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $realName = 'realName';
        $cardId = 'cardId';

        $command = new EditCrewCommand($realName, $cardId, 1);
        $this->assertInstanceof('Base\Sdk\Crew\Command\Crew\EditCrewCommand', $command);
    }
}
