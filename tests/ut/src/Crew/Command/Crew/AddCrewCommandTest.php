<?php
namespace Sdk\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class AddCrewCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $realName = 'realName';
        $cellphone = 'cellphone';
        $password = 'password';
        $cardId = 'cardId';

        $command = new AddCrewCommand($realName, $cellphone, $password, $cardId);
        $this->assertInstanceof('Base\Sdk\Crew\Command\Crew\AddCrewCommand', $command);
    }
}
