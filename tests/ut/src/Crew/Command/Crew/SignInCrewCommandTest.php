<?php
namespace Sdk\Crew\Command\Crew;

use PHPUnit\Framework\TestCase;

class SignInCrewCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $userName = 'userName';
        $password = 'password';

        $command = new SignInCrewCommand($userName, $password);
        $this->assertInstanceof('Base\Sdk\Crew\Command\Crew\SignInCrewCommand', $command);
    }
}
