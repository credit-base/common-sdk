<?php
namespace Sdk\Crew\Adapter\Crew;

use PHPUnit\Framework\TestCase;

class CrewRestfulAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new CrewRestfulAdapter();
        $this->assertInstanceof('Base\Sdk\Crew\Adapter\Crew\CrewRestfulAdapter', $adapter);
    }
}
