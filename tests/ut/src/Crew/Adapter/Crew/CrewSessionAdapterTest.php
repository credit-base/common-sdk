<?php
namespace Sdk\Crew\Adapter\Crew;

use PHPUnit\Framework\TestCase;

class CrewSessionAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new CrewSessionAdapter();
        $this->assertInstanceof('Base\Sdk\Crew\Adapter\Crew\CrewSessionAdapter', $adapter);
    }
}
