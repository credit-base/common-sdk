<?php
namespace Sdk\Member\Model;

use PHPUnit\Framework\TestCase;

class MemberTest extends TestCase
{
    public function testCorrectExtendsModel()
    {
        $model = new Member();
        $this->assertInstanceof('Base\Sdk\Member\Model\Member', $model);
    }
}
