<?php
namespace Sdk\Member\Model;

use PHPUnit\Framework\TestCase;

class NullMemberTest extends TestCase
{
    public function testCorrectExtendsModel()
    {
        $model = new NullMember();
        $this->assertInstanceof('Base\Sdk\Member\Model\NullMember', $model);
    }
}
