<?php
namespace Sdk\Member\Command\Member;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Member\Model\Member;

class SignOutMemberCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        Core::$container->set('member', new Member(1));
        
        $command = new SignOutMemberCommand();
        $this->assertInstanceof('Base\Sdk\Member\Command\Member\SignOutMemberCommand', $command);
    }
}
