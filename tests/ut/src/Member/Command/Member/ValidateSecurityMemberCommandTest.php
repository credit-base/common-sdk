<?php
namespace Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class ValidateSecurityMemberCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $id = 1;
        $securityAnswer = 'securityAnswer';

        $command = new ValidateSecurityMemberCommand(
            $securityAnswer,
            $id
        );
        $this->assertInstanceof('Base\Sdk\Member\Command\Member\ValidateSecurityMemberCommand', $command);
    }
}
