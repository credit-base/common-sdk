<?php
namespace Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class EditMemberCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $gender = 1;
        $id = 1;

        $command = new EditMemberCommand($gender, $id);
        $this->assertInstanceof('Base\Sdk\Member\Command\Member\EditMemberCommand', $command);
    }
}
