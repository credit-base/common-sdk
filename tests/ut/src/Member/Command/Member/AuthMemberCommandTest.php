<?php
namespace Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class AuthMemberCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $id = 1;
        $identify = 'identify';

        $command = new AuthMemberCommand($id, $identify);
        $this->assertInstanceof('Base\Sdk\Member\Command\Member\AuthMemberCommand', $command);
    }
}
