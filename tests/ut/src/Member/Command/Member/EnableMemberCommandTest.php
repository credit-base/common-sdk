<?php
namespace Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class EnableMemberCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $id = 1;

        $command = new EnableMemberCommand($id);
        $this->assertInstanceof('Base\Sdk\Member\Command\Member\EnableMemberCommand', $command);
    }
}
