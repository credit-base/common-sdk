<?php
namespace Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class SignInMemberCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $userName = 'userName';
        $password = 'password';

        $command = new SignInMemberCommand($userName, $password);
        $this->assertInstanceof('Base\Sdk\Member\Command\Member\SignInMemberCommand', $command);
    }
}
