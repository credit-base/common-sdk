<?php
namespace Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class ResetPasswordMemberCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $id = 1;
        $userName = 'userName';
        $securityAnswer = 'securityAnswer';
        $password = 'password';

        $command = new ResetPasswordMemberCommand(
            $userName,
            $securityAnswer,
            $password,
            $id
        );
        $this->assertInstanceof('Base\Sdk\Member\Command\Member\ResetPasswordMemberCommand', $command);
    }
}
