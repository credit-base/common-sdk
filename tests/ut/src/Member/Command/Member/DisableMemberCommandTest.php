<?php
namespace Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class DisableMemberCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $id = 1;

        $command = new DisableMemberCommand($id);
        $this->assertInstanceof('Base\Sdk\Member\Command\Member\DisableMemberCommand', $command);
    }
}
