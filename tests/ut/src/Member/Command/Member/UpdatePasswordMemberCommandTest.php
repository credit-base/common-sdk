<?php
namespace Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class UpdatePasswordMemberCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $id = 1;
        $oldPassword = 'oldPassword';
        $password = 'password';

        $command = new UpdatePasswordMemberCommand(
            $password,
            $oldPassword,
            $id
        );
        $this->assertInstanceof('Base\Sdk\Member\Command\Member\UpdatePasswordMemberCommand', $command);
    }
}
