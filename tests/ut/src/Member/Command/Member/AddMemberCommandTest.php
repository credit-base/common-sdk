<?php
namespace Sdk\Member\Command\Member;

use PHPUnit\Framework\TestCase;

class AddMemberCommandTest extends TestCase
{
    public function testCorrectExtendsCommand()
    {
        $userName = 'userName';
        $realName = 'realName';
        $cardId = 'cardId';
        $cellphone = 'cellphone';
        $email = 'email';
        $contactAddress = 'contactAddress';
        $securityAnswer = 'securityAnswer';
        $password = 'password';
        $securityQuestion = 1;

        $command = new AddMemberCommand(
            $userName,
            $realName,
            $cardId,
            $cellphone,
            $email,
            $contactAddress,
            $securityAnswer,
            $password,
            $securityQuestion
        );
        $this->assertInstanceof('Base\Sdk\Member\Command\Member\AddMemberCommand', $command);
    }
}
