<?php
namespace Sdk\Member\WidgetRules;

use PHPUnit\Framework\TestCase;

class MemberWidgetRulesTest extends TestCase
{
    public function testCorrectExtendsRepository()
    {
        $translator = new MemberWidgetRules();
        $this->assertInstanceof('Base\Sdk\Member\WidgetRules\MemberWidgetRules', $translator);
    }
}
