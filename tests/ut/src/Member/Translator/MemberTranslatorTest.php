<?php
namespace Sdk\Member\Translator;

use PHPUnit\Framework\TestCase;

class MemberTranslatorTest extends TestCase
{
    public function testCorrectExtendsRepository()
    {
        $translator = new MemberTranslator();
        $this->assertInstanceof('Base\Sdk\Member\Translator\MemberTranslator', $translator);
    }
}
