<?php
namespace Sdk\Member\Translator;

use PHPUnit\Framework\TestCase;

class MemberRestfulTranslatorTest extends TestCase
{
    public function testCorrectExtendsRepository()
    {
        $translator = new MemberRestfulTranslator();
        $this->assertInstanceof('Base\Sdk\Member\Translator\MemberRestfulTranslator', $translator);
    }
}
