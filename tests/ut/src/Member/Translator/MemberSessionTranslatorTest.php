<?php
namespace Sdk\Member\Translator;

use PHPUnit\Framework\TestCase;

class MemberSessionTranslatorTest extends TestCase
{
    public function testCorrectExtendsRepository()
    {
        $translator = new MemberSessionTranslator();
        $this->assertInstanceof('Base\Sdk\Member\Translator\MemberSessionTranslator', $translator);
    }
}
