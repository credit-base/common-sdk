<?php
namespace Sdk\Member\Repository;

use PHPUnit\Framework\TestCase;

class MemberSessionRepositoryTest extends TestCase
{
    public function testCorrectExtendsRepository()
    {
        $repository = new MemberSessionRepository();
        $this->assertInstanceof('Base\Sdk\Member\Repository\MemberSessionRepository', $repository);
    }
}
