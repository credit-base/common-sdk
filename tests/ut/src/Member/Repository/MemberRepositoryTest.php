<?php
namespace Sdk\Member\Repository;

use PHPUnit\Framework\TestCase;

class MemberRepositoryTest extends TestCase
{
    public function testCorrectExtendsRepository()
    {
        $repository = new MemberRepository();
        $this->assertInstanceof('Base\Sdk\Member\Repository\MemberRepository', $repository);
    }
}
