<?php
namespace Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class AddMemberCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new AddMemberCommandHandler();
        $this->assertInstanceof('Base\Sdk\Member\CommandHandler\Member\AddMemberCommandHandler', $commandHandler);
    }
}
