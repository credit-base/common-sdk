<?php
namespace Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class SignInMemberCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new SignInMemberCommandHandler();
        $this->assertInstanceof(
            'Base\Sdk\Member\CommandHandler\Member\SignInMemberCommandHandler',
            $commandHandler
        );
    }
}
