<?php
namespace Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class AuthMemberCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new AuthMemberCommandHandler();
        $this->assertInstanceof('Base\Sdk\Member\CommandHandler\Member\AuthMemberCommandHandler', $commandHandler);
    }
}
