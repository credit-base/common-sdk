<?php
namespace Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class EnableMemberCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new EnableMemberCommandHandler();
        $this->assertInstanceof('Base\Sdk\Member\CommandHandler\Member\EnableMemberCommandHandler', $commandHandler);
    }
}
