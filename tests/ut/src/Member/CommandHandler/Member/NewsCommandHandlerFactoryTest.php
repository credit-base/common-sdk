<?php
namespace Sdk\Member\CommandHandler\Member;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Member\Model\Member;
use Sdk\Member\Command\Member\AddMemberCommand;
use Sdk\Member\Command\Member\AuthMemberCommand;
use Sdk\Member\Command\Member\EditMemberCommand;
use Sdk\Member\Command\Member\EnableMemberCommand;
use Sdk\Member\Command\Member\SignInMemberCommand;
use Sdk\Member\Command\Member\SignOutMemberCommand;
use Sdk\Member\Command\Member\DisableMemberCommand;
use Sdk\Member\Command\Member\ResetPasswordMemberCommand;
use Sdk\Member\Command\Member\UpdatePasswordMemberCommand;
use Sdk\Member\Command\Member\ValidateSecurityMemberCommand;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class MemberCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new MemberCommandHandlerFactory();
    }

    public function testEnableMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EnableMemberCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Member\CommandHandler\Member\EnableMemberCommandHandler',
            $commandHandler
        );
    }

    public function testDisableMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableMemberCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Member\CommandHandler\Member\DisableMemberCommandHandler',
            $commandHandler
        );
    }

    public function testAddMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddMemberCommand(
                $this->faker->name(),
                $this->faker->name(),
                $this->faker->creditCardNumber(),
                $this->faker->phoneNumber(),
                $this->faker->email(),
                $this->faker->address(),
                $this->faker->word(),
                md5($this->faker->password()),
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Member\CommandHandler\Member\AddMemberCommandHandler',
            $commandHandler
        );
    }

    public function testEditMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditMemberCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Member\CommandHandler\Member\EditMemberCommandHandler',
            $commandHandler
        );
    }

    public function testResetPasswordMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ResetPasswordMemberCommand(
                $this->faker->name(),
                $this->faker->word(),
                md5($this->faker->password()),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Member\CommandHandler\Member\ResetPasswordMemberCommandHandler',
            $commandHandler
        );
    }

    public function testSignInMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new SignInMemberCommand(
                $this->faker->name(),
                $this->faker->password(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Member\CommandHandler\Member\SignInMemberCommandHandler',
            $commandHandler
        );
    }

    public function testUpdatePasswordMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new UpdatePasswordMemberCommand(
                $this->faker->password(),
                $this->faker->password(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Member\CommandHandler\Member\UpdatePasswordMemberCommandHandler',
            $commandHandler
        );
    }

    public function testAuthMemberCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AuthMemberCommand(
                $this->faker->randomNumber(),
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Member\CommandHandler\Member\AuthMemberCommandHandler',
            $commandHandler
        );
    }

    public function testSignOutMemberCommandHandler()
    {
        Core::$container->set('member', new Member(1));

        $commandHandler = $this->commandHandler->getHandler(
            new SignOutMemberCommand()
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\Member\CommandHandler\Member\SignOutMemberCommandHandler',
            $commandHandler
        );
    }
}
