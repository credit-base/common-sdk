<?php
namespace Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class EditMemberCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new EditMemberCommandHandler();
        $this->assertInstanceof('Base\Sdk\Member\CommandHandler\Member\EditMemberCommandHandler', $commandHandler);
    }
}
