<?php
namespace Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class UpdatePasswordMemberCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new UpdatePasswordMemberCommandHandler();
        $this->assertInstanceof(
            'Base\Sdk\Member\CommandHandler\Member\UpdatePasswordMemberCommandHandler',
            $commandHandler
        );
    }
}
