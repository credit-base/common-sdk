<?php
namespace Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class DisableMemberCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new DisableMemberCommandHandler();
        $this->assertInstanceof('Base\Sdk\Member\CommandHandler\Member\DisableMemberCommandHandler', $commandHandler);
    }
}
