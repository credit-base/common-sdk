<?php
namespace Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class ValidateSecurityMemberCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new ValidateSecurityMemberCommandHandler();
        $this->assertInstanceof(
            'Base\Sdk\Member\CommandHandler\Member\ValidateSecurityMemberCommandHandler',
            $commandHandler
        );
    }
}
