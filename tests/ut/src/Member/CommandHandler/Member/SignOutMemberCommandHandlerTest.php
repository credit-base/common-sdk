<?php
namespace Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class SignOutMemberCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new SignOutMemberCommandHandler();
        $this->assertInstanceof(
            'Base\Sdk\Member\CommandHandler\Member\SignOutMemberCommandHandler',
            $commandHandler
        );
    }
}
