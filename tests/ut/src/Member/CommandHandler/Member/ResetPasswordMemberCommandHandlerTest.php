<?php
namespace Sdk\Member\CommandHandler\Member;

use PHPUnit\Framework\TestCase;

class ResetPasswordMemberCommandHandlerTest extends TestCase
{
    public function testCorrectExtendsCommandHandler()
    {
        $commandHandler = new ResetPasswordMemberCommandHandler();
        $this->assertInstanceof(
            'Base\Sdk\Member\CommandHandler\Member\ResetPasswordMemberCommandHandler',
            $commandHandler
        );
    }
}
