<?php
namespace Sdk\Member\Adapter\Member;

use PHPUnit\Framework\TestCase;

class MemberRestfulAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new MemberRestfulAdapter();
        $this->assertInstanceof('Base\Sdk\Member\Adapter\Member\MemberRestfulAdapter', $adapter);
    }
}
