<?php
namespace Sdk\Member\Adapter\Member;

use PHPUnit\Framework\TestCase;

class MemberSessionAdapterTest extends TestCase
{
    public function testCorrectExtendsAdapter()
    {
        $adapter = new MemberSessionAdapter();
        $this->assertInstanceof('Base\Sdk\Member\Adapter\Member\MemberSessionAdapter', $adapter);
    }
}
