<?php
namespace Sdk\News\CommandHandler\UnAuditNews;

use PHPUnit\Framework\TestCase;
use Sdk\News\Command\UnAuditNews\EditUnAuditNewsCommand;
use Sdk\News\Command\UnAuditNews\ApproveUnAuditNewsCommand;
use Sdk\News\Command\UnAuditNews\RejectUnAuditNewsCommand;

use Sdk\News\CommandHandler\NewsCommondHandlerDataTrait;

class UnAuditNewsCommandHandlerFactoryTest extends TestCase
{
    use NewsCommondHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new UnAuditNewsCommandHandlerFactory();
    }

    public function testEditUnAuditNewsCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new EditUnAuditNewsCommand(
                $data['title'],
                $data['source'],
                $data['content'],
                $data['newsType'],
                $data['dimension'],
                $data['status'],
                $data['stick'],
                $data['bannerStatus'],
                $data['homePageShowStatus'],
                $data['cover'],
                $data['attachments'],
                $data['bannerImage'],
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\News\CommandHandler\UnAuditNews\EditUnAuditNewsCommandHandler',
            $addCommandHandler
        );
    }

    public function testApproveUnAuditNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ApproveUnAuditNewsCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\CommandHandler\UnAuditNews\ApproveUnAuditNewsCommandHandler',
            $commandHandler
        );
    }

    public function testRejectUnAuditNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RejectUnAuditNewsCommand(
                '驳回原因',
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\CommandHandler\UnAuditNews\RejectUnAuditNewsCommandHandler',
            $commandHandler
        );
    }
}
