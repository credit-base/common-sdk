<?php
namespace Sdk\News\CommandHandler\News;

use PHPUnit\Framework\TestCase;
use Sdk\News\Command\News\AddNewsCommand;
use Sdk\News\Command\News\EnableNewsCommand;
use Sdk\News\Command\News\DisableNewsCommand;
use Sdk\News\Command\News\TopNewsCommand;
use Sdk\News\Command\News\CancelTopNewsCommand;
use Sdk\News\Command\News\MoveNewsCommand;

use Sdk\News\CommandHandler\NewsCommondHandlerDataTrait;

class NewsCommandHandlerFactoryTest extends TestCase
{
    use NewsCommondHandlerDataTrait;

    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new NewsCommandHandlerFactory();
    }

    public function testAddNewsCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $addCommandHandler = $this->commandHandler->getHandler(
            new AddNewsCommand(
                $data['title'],
                $data['source'],
                $data['content'],
                $data['newsType'],
                $data['dimension'],
                $data['status'],
                $data['stick'],
                $data['bannerStatus'],
                $data['homePageShowStatus'],
                $data['cover'],
                $data['attachments'],
                $data['bannerImage']
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $addCommandHandler);
        $this->assertInstanceOf(
            'Sdk\News\CommandHandler\News\AddNewsCommandHandler',
            $addCommandHandler
        );
    }

    public function testEnableNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EnableNewsCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\CommandHandler\News\EnableNewsCommandHandler',
            $commandHandler
        );
    }

    public function testDisableNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableNewsCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\CommandHandler\News\DisableNewsCommandHandler',
            $commandHandler
        );
    }

    public function testTopNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new TopNewsCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\CommandHandler\News\TopNewsCommandHandler',
            $commandHandler
        );
    }

    public function testCancelTopNewsCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new CancelTopNewsCommand(
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\CommandHandler\News\CancelTopNewsCommandHandler',
            $commandHandler
        );
    }

    public function testMoveNewsCommandHandler()
    {
        $data = $this->getRequestCommonData();
        $commandHandler = $this->commandHandler->getHandler(
            new MoveNewsCommand(
                $data['newsType'],
                $this->faker->randomNumber()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\News\CommandHandler\News\MoveNewsCommandHandler',
            $commandHandler
        );
    }
}
