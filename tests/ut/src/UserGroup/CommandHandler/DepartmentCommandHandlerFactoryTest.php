<?php
namespace Sdk\UserGroup\CommandHandler;

use PHPUnit\Framework\TestCase;
use Sdk\UserGroup\Command\EditDepartmentCommand;

class DepartmentCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new DepartmentCommandHandlerFactory();
    }

    public function testSignInUserGroupCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditDepartmentCommand(
                $this->faker->randomNumber(),
                $this->faker->name
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'Sdk\UserGroup\CommandHandler\EditDepartmentCommandHandler',
            $commandHandler
        );
    }
}
